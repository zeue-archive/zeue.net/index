<?php
  require "../vendor/autoload.php";
  use Symfony\Component\Yaml\Yaml;
  $loadYaml = Yaml::parseFile('../inc/entries.yaml');
  $sticky = $loadYaml["sticky"];
  $unsticky = $loadYaml["unsticky"];
  shuffle($unsticky);
  $entries = array_merge($sticky, $unsticky);
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Zeue Network Index</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link rel="stylesheet" href="assets/css/index.css">
  </head>
  <body>
    <div class="container">
      <div class="row">
          <div id="entries">
            <input class="search" placeholder="Search"/>
            <!-- <b>Sort By: </b>
            <button class="sort" data-sort="name">
              Name
            </button>
            <button class="sort" data-sort="name">
              Description
            </button> -->
            <ul class="list">
              <?php include "../inc/getEntries.func.php";?>
            </ul>
          </div>
      </div>
    </div>
    <script src="assets/js/list.js"></script>
    <script>
      var options = {
        valueNames: [ 'name', 'description' ]
      };
      var userList = new List('entries', options);
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </body>
</html>
