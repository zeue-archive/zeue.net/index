<?php
  function getCardColours() {
    $_presets = [
      [ "blue", false ], // [ COLOUR , WHITE_TEXT? ]
      [ "red", false ],
      [ "grey", false ],
      [ "pink", false ],
      [ "purple", true ],
      [ "deep-purple", true ],
      [ "indigo", true ],
      [ "light-blue", false ],
      [ "cyan", false ],
      [ "teal", true ],
      [ "green", true ],
      [ "light-green", false ],
      [ "lime", false ],
      [ "yellow", false ],
      [ "amber", false ],
      [ "orange", false ],
      [ "deep-orange", false ],
      [ "brown", true ],
      [ "blue-grey", true ],
    ];

    return $_presets[array_rand($_presets, 1)];
  }

  for ($i=0; $i < count($entries); $i++) {
    $cardColors = getCardColours();
?>
  <li>
    <div class="card <?php echo $cardColors[0];?>">
      <div class="card-content <?php if ($cardColors[1]) { echo "white-text"; }?>">
        <span class="card-title name">
          <b><?php echo $entries[$i]["name"];?></b>
        </span>
        <p class="description">
          <?php echo $entries[$i]["description"];?>
        </p>
      </div>
      <div class="card-action">
        <?php
          $alc = [
            $cardColors[0]." darken-3",
            $cardColors[0]." darken-4",
          ]; // alternating link-button colours

          for ($j=0; $j < count($entries[$i]["links"]); $j++) {
            $_alc = ($j % 2 == 0) ? $alc[0] : $alc[1];
            $link = $entries[$i]["links"][$j];
        ?>
          <a class="btn <?php echo $_alc;?>" href="<?php echo $link["url"];?>" target="_blank">
            <?php echo $link["text"];?>
          </a>
        <?php } ?>
      </div>
    </div>
  </li>
<?php
  }
?>
