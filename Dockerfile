FROM composer:latest

COPY . ./index.zeue.net

RUN cd index.zeue.net && composer install

EXPOSE 80/tcp

CMD cd index.zeue.net && bin/start
